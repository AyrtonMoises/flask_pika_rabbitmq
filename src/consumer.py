import time

from app import create_app
from ext.pika_connection import create_pika_connection
from blueprints.votacao.doa import VotoEleicaoCRUD


def receber_mensagens(queue_name):

    while True:
        connection = create_pika_connection()
        channel = connection.channel()

        votos_lote = []
        itens_fila = []

        # busca 30 primeiros registros para processar
        for i in range(30):
            method_frame, header_frame, body = channel.basic_get(queue=queue_name, auto_ack=False)
            if method_frame:

                voto_body = body.decode()
                votos_lote.append(voto_body)
                itens_fila.append(method_frame)
                
                print(f'Mensagem {i+1}: {voto_body}')
            else:
                print('Não há mais mensagens na fila')
                break

        try:
            VotoEleicaoCRUD.save_voto(votos_lote)

            # Libera mensagens da fila
            for item_fila in itens_fila:
                channel.basic_ack(item_fila.delivery_tag)

        except Exception as e:
            print(str(e))
            print('Mensagens continuarão na fila')

        
        connection.close()

        # tempo para nova busca
        time.sleep(10)



if __name__ == '__main__':    
    app = create_app()

    with app.app_context():
        receber_mensagens('exemplo_fila')


