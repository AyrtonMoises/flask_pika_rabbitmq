import pika


RABBITMQ_URL = 'amqp://localhost:5672'


def create_pika_connection():
    return pika.BlockingConnection(pika.URLParameters(RABBITMQ_URL))