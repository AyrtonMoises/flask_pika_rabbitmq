import datetime

from flask import request, render_template, redirect, flash, url_for

from ext.cache import cache
from ext.pika_connection import create_pika_connection

from blueprints.votacao.doa import EleicaoCrud, CandidatoCRUD, VotoEleicaoCRUD

from blueprints.votacao import bp_votacao


@bp_votacao.before_app_first_request
def update_status_eleicao():
    eleicao = EleicaoCrud.get_eleicao_ativa()
    
    if eleicao.status:
        cache.set('eleicao_ativa', True)

@bp_votacao.route('/', methods=['GET'])
@cache.cached(timeout=10)
def index():
    resultado_eleicao = VotoEleicaoCRUD.resultado_eleicao()
    return render_template('index.html', resultado_eleicao=resultado_eleicao)

@bp_votacao.route('/eleicao/novo', methods=['GET','POST'])
def eleicao_novo():
    if request.method == 'POST':
        try:
            eleicao = EleicaoCrud.save_eleicao(
                request.form
            )

            if eleicao.status:
                cache.set('eleicao_ativa', True)

            flash('Eleição criada!', 'success')
            return redirect('/eleicoes')
        except Exception as e:
            flash(str(e), 'danger')
            return redirect(url_for('votacao.eleicao_novo'))

    return render_template('form_eleicao.html')

@bp_votacao.route('/eleicoes', methods=['GET'])
def eleicoes():
    eleicoes = EleicaoCrud.all_eleicoes()
    return render_template('eleicoes.html', eleicoes=eleicoes)

@bp_votacao.route('/eleicao/deletar', methods=['POST'])
def eleicao_deletar():
    EleicaoCrud.delete_eleicao(request.form['id'])
    flash('Removido com sucesso!', 'success')
    return redirect('/eleicoes')

@bp_votacao.route('/candidato/novo', methods=['GET','POST'])
def candidato_novo():
    if request.method == 'POST':
        try:
            CandidatoCRUD.save_candidato(
                request.form
            )
            flash('Candidato criado!', 'success')
            return redirect('/candidatos')
        except Exception as e:
            flash(str(e), 'danger')
            return redirect(url_for('votacao.candidato_novo'))

    return render_template('form_candidato.html')

@bp_votacao.route('/candidatos', methods=['GET'])
def candidatos():
    candidatos = CandidatoCRUD.all_candidatos()
    return render_template('candidatos.html', candidatos=candidatos)

@bp_votacao.route('/candidato/deletar', methods=['POST'])
def candidato_deletar():
    try:
        CandidatoCRUD.delete_candidato(request.form['id'])
        flash('Removido com sucesso!', 'success')
        return redirect('/candidatos')
    except Exception as e:
        flash(str(e), 'danger')
        return redirect('/candidatos')


@bp_votacao.route('/eleicao/<id_eleicao>/candidatos', methods=['GET'])
def eleicao_candidatos(id_eleicao):
    candidaturas = EleicaoCrud.get_candidatos_eleicao(id_eleicao)
    return render_template('candidatos_eleicao.html', candidaturas=candidaturas, id_eleicao=id_eleicao)

@bp_votacao.route('/eleicao/<id_eleicao>/candidato/novo', methods=['POST','GET'])
def eleicao_candidato_novo(id_eleicao):
    if request.method == 'POST':
        try:
            EleicaoCrud.save_candidato_eleicao(
                id_eleicao, request.form
            )
            flash('Candidato vinculado a eleição!', 'success')
            return redirect(url_for('votacao.eleicao_candidatos', id_eleicao=id_eleicao))
        except Exception as e:
            flash(str(e), 'danger')
            return redirect(url_for('votacao.eleicao_candidato_novo', id_eleicao=id_eleicao))
        
    candidatos = CandidatoCRUD.all_candidatos()

    return render_template('form_eleicao_candidato.html', candidatos=candidatos, id_eleicao=id_eleicao)


@bp_votacao.route('/eleicao/<id_eleicao>/candidato/deletar', methods=['POST'])
def candidato_eleicao_deletar(id_eleicao):
    try:
        EleicaoCrud.delete_candidato_eleicao(id_eleicao, request.form['id'])
        flash('Removido com sucesso!', 'success')
        return redirect(url_for('votacao.eleicao_candidatos', id_eleicao=id_eleicao))
    except Exception as e:
        flash(str(e), 'danger')
        return redirect(url_for('votacao.eleicao_candidatos', id_eleicao=id_eleicao))

@bp_votacao.route('/eleicao/altera_status', methods=['POST'])
def change_status_eleicao():
    try:
        id_eleicao = request.form['id_eleicao']
        eleicao = EleicaoCrud.change_status_eleicao(id_eleicao)

        if eleicao.status == False:
            cache.set('eleicao_ativa', False)
        else:
            cache.set('eleicao_ativa', True)

        return redirect('/eleicoes')
    except Exception as e:
        flash(str(e), 'danger')
        return redirect('/eleicoes')
    
@bp_votacao.route('/votar', methods=['GET','POST'])
def votar():
    eleicao_ativa = cache.get('eleicao_ativa')

    if not eleicao_ativa:
        flash('Nenhuma eleição ativa!', 'danger')
        return redirect('/')

    if request.method == 'POST':
        try:
            dados = {}
            dados['candidato_eleicao_id'] = int(request.form['candidato_eleicao_id'])
            dados['datahora'] = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S,%f")
            dados['ip'] = request.remote_addr

            dados_to_string = str(dados)

            # cria conexao, declara e manda dados a fila
            connection = create_pika_connection()
            channel = connection.channel()
            channel.queue_declare(queue='exemplo_fila')

            channel.basic_publish(exchange='', routing_key='exemplo_fila', body=dados_to_string)
            connection.close()

            flash('Voto salvo!', 'success')
            return redirect('/')
        except Exception as e:
            flash(str(e), 'danger')
            return redirect(url_for('votacao.votar'))

    eleicao_candidatos = VotoEleicaoCRUD.candidatos_eleicao()
    return render_template('votar.html', eleicao_candidatos=eleicao_candidatos)
