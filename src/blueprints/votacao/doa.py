import base64
from datetime import datetime
from collections import Counter

from ext.database import db
from app import create_app
from blueprints.votacao.models import Eleicao, Candidato, VotoEleicao, candidatos_eleicao


app = create_app()

@app.before_first_request
def create_database():
     db.create_all()

class EleicaoCrud:

    def get_eleicao(eleicao_id):
        eleicao = Eleicao.query.filter_by(id=eleicao_id).first()
        return eleicao

    def get_eleicao_ativa():
        eleicao = Eleicao.query.filter_by(status=True).first()
        return eleicao

    def all_eleicoes():
        all_eleicoes = Eleicao.query.all()
        return all_eleicoes

    def save_eleicao(dados):
        eleicao = Eleicao.query.filter_by(descricao=dados['descricao']).one_or_none()

        if eleicao:
            raise ValueError('Eleição com essa descrição já existe')
        
        eleicao = Eleicao.query.filter_by(status=True).one_or_none()
        
        if eleicao and dados.get('status', 0, type=bool):
            raise ValueError('Já existe uma eleição ativa!')
        
        eleicao = Eleicao(
            descricao=dados['descricao'],
            status=dados.get('status', 0, type=bool),
        )
        db.session.add(eleicao)
        db.session.commit()

        return eleicao

    def change_status_eleicao(eleicao_id):
        eleicao = Eleicao.query.filter(
            Eleicao.status == True,
            Eleicao.id != eleicao_id
        ).one_or_none()
        
        if eleicao:
            raise ValueError('Já existe uma eleição ativa!')
        
        eleicao = Eleicao.query.filter_by(id=eleicao_id).one_or_none()

        eleicao.status = not eleicao.status

        db.session.commit()

        return eleicao

    def delete_eleicao(eleicao_id):
        eleicao = Eleicao.query.filter_by(id=eleicao_id).first()
        eleicao.candidatos.clear()
        db.session.delete(eleicao)
        db.session.commit()

    def get_candidatos_eleicao(eleicao_id):
        candidaturas = db.session.query(
            candidatos_eleicao.c.id, Candidato.nome
        ).join(
            candidatos_eleicao, Candidato.id == candidatos_eleicao.c.candidato_id
        ).join(Eleicao).filter(Eleicao.id == eleicao_id).all()
        return candidaturas

    def save_candidato_eleicao(eleicao_id, dados):
        candidato_existente = Candidato.query.join(
            Eleicao.candidatos
        ).filter(
            Candidato.id == dados['candidato'],
            Eleicao.id == eleicao_id
        ).all()

        if candidato_existente:
            raise ValueError('Candidato já cadastrado')
        
        eleicao = Eleicao.query.filter_by(id=eleicao_id).first()
        candidato = Candidato.query.filter_by(id=dados['candidato']).first()

        eleicao.candidatos.append(candidato)
        db.session.add(eleicao)
        db.session.commit()


    def delete_candidato_eleicao(eleicao_id, candidato_eleicao):

        eleicao_ativa = Eleicao.query.filter_by(
            status=True,
            id=eleicao_id
        ).first()

        if eleicao_ativa:
            raise ValueError('Eleição ativa não pode remover candidatos')
        
        eleicao = Eleicao.query.filter_by(id=eleicao_id).first()
        candidato = Candidato.query.filter_by(id=candidato_eleicao).first()

        eleicao.candidatos.remove(candidato)
        db.session.commit()


class CandidatoCRUD:

    def get_candidato(candidato_id):
        candidato = Candidato.query.filter_by(id=candidato_id).first()
        return candidato

    def all_candidatos():
        all_candidatos = Candidato.query.all()
        return all_candidatos

    def save_candidato(dados):
        candidato = Candidato.query.filter_by(nome=dados['nome']).one_or_none()

        if candidato:
            raise ValueError('Candidato com esse nome já existe')
        
        candidato = Candidato(
            nome=dados['nome'],
        )
        db.session.add(candidato)
        db.session.commit()

    def delete_candidato(candidato_id):
        candidato = Candidato.query.filter_by(id=candidato_id).first()

        candidato_existente = Candidato.query.join(
            Eleicao.candidatos
        ).filter(Candidato.id == candidato_id).all()

        if candidato_existente:
            raise ValueError('Candidato que participa de eleição não pode ser removido!')

        db.session.delete(candidato)
        db.session.commit()


class VotoEleicaoCRUD:

    def candidatos_eleicao():
        eleicao = Eleicao.query.filter_by(status=True).first()

        if not eleicao:
            return []
        
        all_candidatos_eleicao = db.session.query(
            candidatos_eleicao.c.id, Candidato.nome, candidatos_eleicao.c.total_votos
        ).join(
            candidatos_eleicao, Candidato.id == candidatos_eleicao.c.candidato_id
        ).join(Eleicao).filter(Eleicao.id == eleicao.id).all()

        return all_candidatos_eleicao
    
    def resultado_eleicao():
        eleicao = Eleicao.query.filter_by(status=True).first()

        if not eleicao:
            return []

        total_votos_eleicao = db.session.query(
            db.func.sum(candidatos_eleicao.c.total_votos)
            ).filter(
            candidatos_eleicao.c.eleicao_id == eleicao.id
            ).scalar()
        
        if total_votos_eleicao is None:
            total_votos_eleicao = 0

        candidatos_eleicao_query = db.session.query(
            candidatos_eleicao
        ).filter_by(eleicao_id=eleicao.id)

        candidatos_list = []
        for candidato_eleicao in candidatos_eleicao_query:
            candidato_dict = {}
            
            candidato = Candidato.query.filter_by(
                id=candidato_eleicao.candidato_id
            ).first()

            candidato_dict['nome'] = candidato.nome


            if candidato_eleicao.total_votos:
                candidato_dict['percentual'] = round(
                    candidato_eleicao.total_votos / total_votos_eleicao * 100, 2
                )
            else:
                candidato_dict['percentual'] = 0

            candidatos_list.append(candidato_dict)

        return {'candidatos_eleicao': candidatos_list, 'total_votos_eleicao': total_votos_eleicao}

    def save_voto(dados):
        votos = []

        dados = [eval(dado) for dado in dados]

        for dado in dados:

            datahora_string = dado['datahora']
            datahora_datetime = datetime.strptime(datahora_string, '%Y-%m-%d %H:%M:%S,%f')
            dado['datahora'] = datahora_datetime

            # cria hash simples para identificado unico
            hash_voto = dado['ip'] + datahora_string + str(dado['candidato_eleicao_id'])
            hash_voto_bytes = hash_voto.encode('ascii')
            base64_bytes = base64.b64encode(hash_voto_bytes)
            base64_hash_voto = base64_bytes.decode('ascii')

            votos.append(
                VotoEleicao(
                    ip=dado['ip'],
                    datahora=dado['datahora'],
                    candidato_eleicao_id=dado['candidato_eleicao_id'],
                    hash=base64_hash_voto
                )   
            )

        db.session.bulk_save_objects(votos)

        # atualiza aos votos totais
        totais_por_candidato = Counter(voto_candidato['candidato_eleicao_id'] for voto_candidato in dados)

        ids_candidatos_eleicao = [id_candidato for id_candidato in totais_por_candidato.keys()]

        candidato_lote = db.session.query(
            candidatos_eleicao
        ).filter(candidatos_eleicao.c.id.in_(ids_candidatos_eleicao)).all()

        for candidato_dados in candidato_lote:

            total_votos = totais_por_candidato[candidato_dados.id]
            total_votos += candidato_dados.total_votos

            db.session.query(
                candidatos_eleicao
            ).filter_by(
                id = candidato_dados.id).update(
                {'total_votos': total_votos}
            )

        db.session.commit()