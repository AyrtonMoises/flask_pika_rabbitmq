import datetime

from ext.database import db


candidatos_eleicao = db.Table('candidatos_eleicao',
    db.Column('id', db.Integer, primary_key=True),
    db.Column('candidato_id', db.Integer, db.ForeignKey('candidato.id')),
    db.Column('eleicao_id', db.Integer, db.ForeignKey('eleicao.id')),
    db.Column('total_votos', db.Integer)
)

class Candidato(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nome = db.Column(db.String(150), unique=True, nullable=False)

    def __repr__(self):
        return '<Candidato %r>' % self.nome
    

class Eleicao(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    descricao = db.Column(db.String(100), unique=True, nullable=False)
    status = db.Column(db.Boolean, default=False, nullable=False)
    datahora = db.Column(db.DateTime, default=datetime.datetime.now, nullable=False)
    candidatos = db.relationship('Candidato', secondary=candidatos_eleicao, lazy='subquery',
        backref=db.backref('eleicoes', lazy=True))

    def __repr__(self):
        return '<Eleicao %r>' % self.id
    

class VotoEleicao(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    datahora = db.Column(db.DateTime, nullable=False)
    ip = db.Column(db.String(150), nullable=False)
    hash = db.Column(db.String(150), unique=True, nullable=False)
    candidato_eleicao_id = db.Column(db.Integer, db.ForeignKey('candidatos_eleicao.id')) 

    def __repr__(self):
        return '<VotoEleicao %r>' % self.id

    