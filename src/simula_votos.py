import time
import threading
import requests


def run():
    ids_candidatos = [4, 5, 6] # ids das candidaturas na eleicao 
    while True:
        for candidato in ids_candidatos:
            try:
                target = 'http://localhost:5000/votar'
                requests.post(target, data={'candidato_eleicao_id': candidato}, timeout=1)

            except requests.RequestException:
                print("cannot connect")
                time.sleep(1)


if __name__ == "__main__":
    thread = threading.Thread(target=run)
    thread.daemon = True
    thread.start()

    while True:
        time.sleep(3)