from flask import Flask


def create_app():
    app = Flask(__name__)
    app.config['SECRET_KEY'] = 'secret-key'
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///teste.sqlite3'
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
    app.config['CACHE_TYPE'] = "SimpleCache"
    app.config['CACHE_DEFAULT_TIMEOUT'] = 300

    with app.app_context():
        from ext.database import db
        from ext.cache import cache

        db.init_app(app)
        cache.init_app(app)

        from blueprints.votacao import bp_votacao
        app.register_blueprint(bp_votacao, url_prefix='/')
    
    return app


if __name__ == '__main__':    
    app = create_app()

    app.run(
        port=5000,
        debug=True
    )

